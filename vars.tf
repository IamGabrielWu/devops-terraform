variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {
  default="us-east-1"
}
variable "AMIS" {
  type ="map"
  default = {
    ap-northeast-1="ami-c0c64ba6"
    ap-south-1="ami-43d0982c"
    ap-southeast-1="ami-2db2d251"
    ca-central-1="ami-cbb802af"
    eu-central-1="ami-1fbe3570"
    eu-west-1="ami-94b236ed"
    sa-east-1="ami-3181c75d"
    us-east-1="ami-5cd4a126"
    us-west-1="ami-e1131781"
    ap-northeast-1="ami-7cc74a1a"
    ap-south-1="ami-08d49c67"
    ap-south-1="ami-24de964b"
  }
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "/home/gabrielwu/.ssh/id_rsa"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "/home/gabrielwu/.ssh/id_rsa.pub"
}
variable "instance_username" {
  default = "ubuntu"
}
