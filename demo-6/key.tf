resource "aws_key_pair" "gabrielkey" {
    key_name ="gabrielkey"
    public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
}
