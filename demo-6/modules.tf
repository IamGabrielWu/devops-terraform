module "consul" {
  source="github.com/wardviaene/terraform-consul-module.git"
  key_name="${aws_key_pair.gabrielkey.key_name}"
  key_path="${var.PATH_TO_PRIVATE_KEY}"
  subnets={
    "0"="subnet-3fe9e202",
    "1"="subnet-68bf0264"
    "2"="subnet-a0cbcef8"
  }
  vpc_id="vpc-56ceaf31"
}
output "consul-output" {
  value="${module.consul.server_address}"
}
