data "template_file" "my-template" {
  template="${file("templates/init.tpl")}"
  vars {
    myip="${aws_instance.database1.private_ip}"
  }
}
